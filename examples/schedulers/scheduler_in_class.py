from datetime import datetime, timedelta
import time
import os
import logging

from apscheduler.schedulers.blocking import BlockingScheduler
logging.basicConfig(filename='example.log', level=logging.DEBUG)


class Test(object):

    @classmethod
    def wolololo2(cls):
        print("I'm a class method of %s " % cls)


def wolololo():
    print("I'm function")


class LocationSystem(object):

    def __init__(self, *args, **kwargs):
        super(LocationSystem, self).__init__(*args, **kwargs)
        self.location = {"x": 0, "y": 0}
        self.scheduler = BlockingScheduler()
        self.scheduler.add_jobstore('mongodb', collection='example_jobs')
        self._init_scheduling_job()

    def __reduce__(self):
        return(self.__class__, ())

    def _init_scheduling_job(self):
        alarm_time = datetime.now() + timedelta(seconds=10)
        self.scheduler.add_job(Test.wolololo2, 'date', run_date=alarm_time)
        self.scheduler.add_job(wolololo, 'date', run_date=alarm_time)
        self.scheduler.add_job(self.display_location, 'date',
                               run_date=alarm_time)
        self.scheduler.add_job(LocationSystem.display_location, 'date',
                               run_date=alarm_time, args=(self,))

    def start_scheduler(self):
        self.scheduler.start()

    def shutdown_scheduler(self):
        self.scheduler.shutdown()

    def display_location(self):
        print("I'm instance method %s" % self)


if __name__ == '__main__':

    system = LocationSystem()
    print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))
    try:
        system.start_scheduler()
    except (KeyboardInterrupt, SystemExit):
        system.shutdown_scheduler()
